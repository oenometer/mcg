#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject

from mcg.zeroconf import ZeroconfProvider




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/connection-panel.ui')
class ConnectionPanel(Gtk.Box):
    __gtype_name__ = 'McgConnectionPanel'
    __gsignals__ = {
        'connection-changed': (GObject.SIGNAL_RUN_FIRST, None, (str, int, str))
    }

    # Widgets
    zeroconf_list = Gtk.Template.Child()
    host_entry = Gtk.Template.Child()
    port_spinner = Gtk.Template.Child()
    password_entry = Gtk.Template.Child()


    def __init__(self):
        super().__init__()
        self._services = Gtk.ListStore(str, str, int)
        self._profile = None

        # Zeroconf
        self.zeroconf_list.set_model(self._services)
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Zeroconf", renderer, text=0)
        self.zeroconf_list.append_column(column)

        # Zeroconf provider
        self._zeroconf_provider = ZeroconfProvider()
        self._zeroconf_provider.connect_signal(ZeroconfProvider.SIGNAL_SERVICE_NEW, self.on_new_service)


    def on_new_service(self, service):
        name, host, port = service
        self._services.append([name, host, port])


    @Gtk.Template.Callback()
    def on_service_selected(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter != None:
            service = model[treeiter]
            self.set_host(service[1])
            self.set_port(service[2])


    @Gtk.Template.Callback()
    def on_zeroconf_list_outfocused(self, widget, event):
        self.zeroconf_list.get_selection().unselect_all()


    @Gtk.Template.Callback()
    def on_host_entry_outfocused(self, widget, event):
        self._call_back()


    @Gtk.Template.Callback()
    def on_port_spinner_value_changed(self, widget):
        self._call_back()


    @Gtk.Template.Callback()
    def on_password_entry_outfocused(self, widget, event):
        self._call_back()


    def set_host(self, host):
        self.host_entry.set_text(host)


    def get_host(self):
        return self.host_entry.get_text()


    def set_port(self, port):
        self.port_spinner.set_value(port)


    def get_port(self):
        return self.port_spinner.get_value_as_int()


    def set_password(self, password):
        if password is None:
            password = ""
        self.password_entry.set_text(password)


    def get_password(self):
        if self.password_entry.get_text() == "":
            return None
        else:
            return self.password_entry.get_text()


    def _call_back(self):
        self.emit('connection-changed', self.get_host(), self.get_port(), self.get_password(),)
