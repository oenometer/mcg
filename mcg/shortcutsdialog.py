#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/shortcuts-dialog.ui')
class ShortcutsDialog(Gtk.ShortcutsWindow):
    __gtype_name__ = 'McgShortcutsDialog'


    def __init__(self):
        super().__init__()
