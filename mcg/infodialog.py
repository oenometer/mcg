#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '3.0')
import logging

from gi.repository import Gtk, GObject, GdkPixbuf




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/info-dialog.ui')
class InfoDialog(Gtk.AboutDialog):
    __gtype_name__ = 'McgInfoDialog'


    def __init__(self):
        super().__init__()

        self._logger = logging.getLogger(__name__)
