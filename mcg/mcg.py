#!/usr/bin/env python3


import sys

from mcg import Environment




def setup_resources():
    from gi.repository import Gio

    resource = Gio.resource_load(
        Environment.get_data('de.coderkun.mcg.gresource')
    )
    Gio.Resource._register(resource)


def setup_locale():
    import gettext

    gettext.bindtextdomain('mcg', Environment.get_locale())
    gettext.textdomain('mcg')


def run_application():
    from mcg.application import Application

    app = Application()
    return app.run(sys.argv)


def main():
    setup_resources()
    setup_locale()
    return run_application()




if __name__ == "__main__":
    main()
    sys.exit(main())
