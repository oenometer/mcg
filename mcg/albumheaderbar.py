#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/album-headerbar.ui')
class AlbumHeaderbar(Gtk.HeaderBar):
    __gtype_name__ = 'McgAlbumHeaderbar'
    __gsignals__ = {
        'close': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    # Widgets
    standalone_title = Gtk.Template.Child()
    standalone_artist = Gtk.Template.Child()


    def __init__(self):
        super().__init__()


    @Gtk.Template.Callback()
    def on_close_clicked(self, widget):
        self.emit('close')


    def set_album(self, album):
        self.standalone_title.set_text(album.get_title())
        self.standalone_artist.set_text(", ".join(album.get_albumartists()))
