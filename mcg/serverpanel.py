#!/usr/bin/env python3


import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/server-toolbar.ui')
class ServerToolbar(Gtk.ButtonBox):
    __gtype_name__ = 'McgServerToolbar'


    def __init__(self):
        super().__init__()




@Gtk.Template(resource_path='/de/coderkun/mcg/ui/server-panel.ui')
class ServerPanel(Gtk.Box):
    __gtype_name__ = 'McgServerPanel'
    __gsignals__ = {
        'change-output-device': (GObject.SIGNAL_RUN_FIRST, None, (GObject.TYPE_PYOBJECT,bool,)),
    }

    # Widgets
    # Status widgets
    status_file = Gtk.Template.Child()
    status_audio = Gtk.Template.Child()
    status_bitrate = Gtk.Template.Child()
    status_error = Gtk.Template.Child()
    # Stats widgets
    stats_artists = Gtk.Template.Child()
    stats_albums = Gtk.Template.Child()
    stats_songs = Gtk.Template.Child()
    stats_dbplaytime = Gtk.Template.Child()
    stats_playtime = Gtk.Template.Child()
    stats_uptime = Gtk.Template.Child()
    # Audio ouptut devices widgets
    output_devices = Gtk.Template.Child()


    def __init__(self):
        super().__init__()
        self._none_label = ""
        self._output_buttons = {}
        self._is_selected = False

        # Widgets
        self._toolbar = ServerToolbar()
        self._none_label = self.status_file.get_label()


    def set_selected(self, selected):
        self._is_selected = selected


    def get_toolbar(self):
        return self._toolbar


    def on_output_device_toggled(self, widget, device):
        self.emit('change-output-device', device, widget.get_active())


    def set_status(self, file, audio, bitrate, error):
        if file:
            file = GObject.markup_escape_text(file)
        else:
            file = self._none_label
        self.status_file.set_markup(file)
        # Audio information
        if  audio:
            parts = audio.split(":")
            if len(parts) == 3:
                audio = "{} Hz, {} bit, {} channels".format(parts[0], parts[1], parts[2])
        else:
            audio = self._none_label
        self.status_audio.set_markup(audio)
        # Bitrate
        if bitrate:
            bitrate = bitrate + " kb/s"
        else:
            bitrate = self._none_label
        self.status_bitrate.set_markup(bitrate)
        # Error
        if error:
            error = GObject.markup_escape_text(error)
        else:
            error = self._none_label
        self.status_error.set_markup(error)


    def set_stats(self, artists, albums, songs, dbplaytime, playtime, uptime):
        self.stats_artists.set_text(str(artists))
        self.stats_albums.set_text(str(albums))
        self.stats_songs.set_text(str(songs))
        self.stats_dbplaytime.set_text(str(dbplaytime))
        self.stats_playtime.set_text(str(playtime))
        self.stats_uptime.set_text(str(uptime))


    def set_output_devices(self, devices):
        device_ids = []

        # Add devices
        for device in devices:
            device_ids.append(device.get_id())
            if device.get_id() in self._output_buttons.keys():
                self._output_buttons[device.get_id()].freeze_notify()
                self._output_buttons[device.get_id()].set_active(device.is_enabled())
                self._output_buttons[device.get_id()].thaw_notify()
            else:
                button = Gtk.CheckButton(device.get_name())
                if device.is_enabled():
                    button.set_active(True)
                handler = button.connect('toggled', self.on_output_device_toggled, device)
                self.output_devices.insert(button, -1)
                self._output_buttons[device.get_id()] = button
        self.output_devices.show_all()

        # Remove devices
        for id in self._output_buttons.keys():
            if id not in device_ids:
                self.output_devices.remove(self._output_buttons[id].get_parent())
