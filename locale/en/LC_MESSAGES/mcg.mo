��    F      L  a   |                           &     -     5     C     J     S     b     u     }     �  
   �     �     �  _   �          7     T     r     y          �     �     �     �     �     �     �     �     �  	   �     	                    $     )     >     Q     Y     h     x     �     �  !   �     �     �     �  
   �     �     	     $	     C	     ]	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	      
     
  �  
               "     )     0     8     F     M     V     e     x     �     �  
   �     �     �  _   �     "     :     W     u     |     �     �     �     �     �     �     �     �     �     �  	                       !     '     ,     A     T     \     d     t     �     �  !   �  )   �     �  
   �  
                  4     S     m     �     �     �     �     �     �     �     �  	   �     �     �     �                    $   )      (   !   -            *       6      0                          B   8   C                 ?         7                          #           2              &   1      "      D   .            %   A         
       @   '   ,         5   <   ;   	   +      3      9                             4   :   >   =       E   F           /           <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter URL or local path Enter hostname or IP address Enter password or leave blank Error: File: General Host: Image Directory: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search the library Seconds Seconds played Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts Songs Sort Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Library panel Switch to the Playlist panel Title Toggle Fullscreen cancel play queue remove search library sort by artist sort by title sort by year {} feat. {} {}:{} minutes Project-Id-Version: CoverGrid (mcg)
PO-Revision-Date: 2020-07-25 14:17+0200
Last-Translator: coderkun <olli@suruatoel.xyz>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: data/gtk.glade
X-Poedit-SearchPath-1: data/gtk.menu.ui
X-Poedit-SearchPath-2: data/gtk.shortcuts.ui
X-Poedit-SearchPath-3: mcg
 <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter URL or local path Enter hostname or IP address Enter password or leave blank Error: File: General Host: Image Directory: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search the library Seconds Seconds Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts (this dialog) Songs Sort order Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Cover panel Switch to the Playlist panel Title Toggle fullscreen cancel play queue remove search library by Artist by Title by Year {} feat. {} {}:{} minutes 