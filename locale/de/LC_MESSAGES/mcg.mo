��    F      L  a   |                           &     -     5     C     J     S     b     u     }     �  
   �     �     �  _   �          7     T     r     y          �     �     �     �     �     �     �     �     �  	   �     	                    $     )     >     Q     Y     h     x     �     �  !   �     �     �     �  
   �     �     	     $	     C	     ]	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	      
     
    
     "     0     I  	   O  	   Y     c     p     w     �     �  	   �  &   �     �  
   �     �     �  w        �  "   �  "   �     �     �  	   �                      
   ,     7     I     ^     t  	   �  	   �     �     �     �     �     �     �          
          -     F     M  #   h  0   �     �  
   �     �     �  %   �          &     @  #   _     �     �  	   �  	   �  	   �  	   �     �     �  
   �  	   �  	   �                    $   )      (   !   -            *       6      0                          B   8   C                 ?         7                          #           2              &   1      "      D   .            %   A         
       @   '   ,         5   <   ;   	   +      3      9                             4   :   >   =       E   F           /           <i>none</i> Adjust the volume Albums Artist Artists Audio Devices Audio: Bitrate: Clear Playlist Clear the playlist Connect Connect or disconnect Connect to MPD Connection Cover Cover Panel CoverGrid is a client for the Music Player Daemon, focusing on albums instead of single tracks. Enter URL or local path Enter hostname or IP address Enter password or leave blank Error: File: General Host: Image Directory: Info Keyboard Shortcuts Library Library Panel Loading albums Loading images Open the info dialog Password: Play Player Playlist Port: Quit Quit the application Search the library Seconds Seconds played Seconds running Select multiple albums Server Settings and actions Show the cover in fullscreen mode Show the keyboard shortcuts Songs Sort Statistics Status Switch between play and pause Switch to the Connection panel Switch to the Cover panel Switch to the Library panel Switch to the Playlist panel Title Toggle Fullscreen cancel play queue remove search library sort by artist sort by title sort by year {} feat. {} {}:{} minutes Project-Id-Version: CoverGrid (mcg)
PO-Revision-Date: 2020-07-25 14:17+0200
Last-Translator: coderkun <olli@suruatoel.xyz>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: data/gtk.glade
X-Poedit-SearchPath-1: data/gtk.menu.ui
X-Poedit-SearchPath-2: data/gtk.shortcuts.ui
X-Poedit-SearchPath-3: mcg
 <i>nichts</i> Die Lautstärke anpassen Alben Künstler Künstler Audiogeräte Audio: Bitrate: Playlist leeren Die Wiedergabeliste leeren Verbinden Die Verbindung herstellen oder trennen Zu MPD verbinden Verbindung Cover Cover-Paneel CoverGrid ist ein ein Client für den Music Player Daemon, der sich auf Alben anstellen von einzelnen Songs fokussiert. URL oder lokalen Pfad eingeben Hostnamen oder IP-Adresse eingeben Passwort eingeben oder leer lassen Fehler: Datei: Allgemein Host: Bildordner: Info Tastenkombinationen Bibliothek Bibliothekspaneel Alben werden geladen Bilder werden geladen Den Infodialog öffnen Passwort: Abspielen Wiedergabeprogramm Wiedergabeliste Port: Beenden Die Anwendung beenden Die Bibliothek durchsuchen Sekunden Sekunden gespielt Sekunden laufend Mehrere Alben auswählen Server Einstellungen und Aktionen Das Cover im Vollbildmodus anzeigen Die Tastenkombinationen anzeigen (dieser Dialog) Songs Sortierung Statistiken Status Zwischen Abspielen und Pause wechseln Zum Verbindungspaneel wechseln Zum Cover-Paneel wechseln Zum Bibliothekspaneel wechseln Zum Wiedergabelistenpaneel wechseln Titel Vollbild wechseln abbrechen abspielen einreihen entfernen Bibliothek durchsuchen nach Künstler nach Titel nach Jahr {} mit {} {}:{} Minuten 